﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Runtime;
using Android.Views;

namespace NFL_News
{
    [Activity(Label = "NFL News",
        MainLauncher = true,
        Icon = "@drawable/nfl_icon_blue",
        Theme = "@android:style/Theme.Material.Light.DarkActionBar")]
    public class MainActivity : Activity
    {
        private Rootobject rootObj;
        private List<Article> articles;
        //private WebClient wClient;
        private Uri uri;

        private ProgressBar pgBar;
        private TextView txtCarregando;
        private TextView txtStatus;
        private TextView txtTotalResults;
        private ListView lsAdptArticle;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            pgBar = FindViewById<ProgressBar>(Resource.Id.progressBar1);
            txtCarregando = FindViewById<TextView>(Resource.Id.txtCarregando);
            txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            txtTotalResults = FindViewById<TextView>(Resource.Id.txtTotalResults);
            lsAdptArticle = FindViewById<ListView>(Resource.Id.listView1);
            lsAdptArticle.ItemClick += LsAdptArticle_ItemClick;

            //rootObj = new Rootobject();

            Toast.MakeText(this, "Carregando as últimas notícias NFL", ToastLength.Long).Show();

            uri = new Uri(
                Resources.GetString(Resource.String.NFL_TOPHEADLINES)
                + "&" +
                Resources.GetString(Resource.String.APIKEY)
            );
            
            Console.WriteLine("URL_NFL-NEWS: " + uri.ToString() +"\n");
        }

        protected override void OnStart()
        {
            base.OnStart();
            this.LoadMainNews();
            //this.LoadDataWebClient();
        }

        //opção 1
        //Carrega as noticias na tela principal usando HttpClient
        private async void LoadMainNews()
        {
            using (var client = new HttpClient())
            {
                //client.Timeout = TimeSpan.Parse("1000");
                try
                {                        
                    var strJson = await client.GetStringAsync(uri);
                    byte[] bt = Encoding.GetEncoding("UTF8").GetBytes(strJson);
                    strJson = Encoding.UTF8.GetString(bt);

                    rootObj = JsonConvert.DeserializeObject<Rootobject>(strJson);
                    txtStatus.Text = rootObj.status;
                    txtTotalResults.Text = rootObj.totalResults.ToString();
                    articles = new List<Article>();
                    articles.AddRange(rootObj.articles);
                    if (articles.Count > 0)
                    {
                        lsAdptArticle.Adapter = new Article_ListViewAdapter(this, articles);
                        pgBar.Visibility = ViewStates.Gone;
                        txtCarregando.Visibility = ViewStates.Gone;
                    }
                    Console.WriteLine("HttpClient: rootObj->STATUS" + rootObj.status + "\nrootObj->TOTALRESULTS" + rootObj.totalResults);
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine("HttpClient Erro: " + ex.Message );
                }
            }
        }

        //Opção 2
        //Carrega as noticias na tela principal usando WebClient
        private void LoadDataWebClient()
        {
            try
            {
                var client = new WebClient();                
                client.DownloadStringAsync(uri);
                client.DownloadDataCompleted += (s, e) =>
                {
                    string strJson = Encoding.UTF8.GetString(e.Result);
                    rootObj = JsonConvert.DeserializeObject<Rootobject>(strJson);
                    txtStatus.Text = rootObj.status;
                    txtTotalResults.Text = rootObj.totalResults.ToString();
                    articles = new List<Article>();
                    articles.AddRange(rootObj.articles);
                    if (articles.Count > 0)
                    {
                        lsAdptArticle.Adapter = new Article_ListViewAdapter(this, articles);
                        pgBar.Visibility = ViewStates.Gone;
                        txtCarregando.Visibility = ViewStates.Gone;
                    }
                    Console.WriteLine("WebClient: rootObj->STATUS" + rootObj.status + "\nrootObj->TOTALRESULTS" + rootObj.totalResults);
                };                
            }
			catch (HttpRequestException ex)
			{
				Console.WriteLine("WebClient Erro: " + ex.Message );
			}
        }

        //Ao clicar em um dos itens da lista
        private void LsAdptArticle_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //var ls = sender as ListView;
            var artigo = articles[e.Position];
            string[] arr =
                        {
                            artigo.author,
                            artigo.description,
                            artigo.publishedAt.ToLongDateString(),
                            artigo.title,
                            artigo.url,
                            artigo.urlToImage
                        };

            Bundle bld = new Bundle();
            bld.PutStringArray("artigo", arr);

            Intent intent = new Intent(this, typeof(Article_Activity));
            intent.PutExtras(bld);
            StartActivity(intent);
        }
    }
}

// Opção para as imagens from URL
//WebClient wc = new WebClient(); //USAR HttpClient
//byte[] bytes = wc.DownloadData("http://localhost/image.gif");
//MemoryStream ms = new MemoryStream(bytes);
//System.Drawing.Image img = System.Drawing.Image.FromStream(ms);