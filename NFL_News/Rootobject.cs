﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using Android.Graphics;
using System.Net;
using Android.Content.Res;

namespace NFL_News
{

    public class Rootobject
    {
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("totalResults")]
        public int totalResults { get; set; }
        [JsonProperty("articles")]
        public List<Article> articles { get; set; }
    }

    public class Article
    {
        [JsonProperty("source")]
        public Source source { get; set; }
        [JsonProperty("author")]
        public string author { get; set; }
        [JsonProperty("title")]
        public string title { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("url")]
        public string url { get; set; }
        [JsonProperty("urlToImage")]
        public string urlToImage { get; set; }
        [JsonProperty("publishedAt")]
        public DateTime publishedAt { get; set; }

        public Bitmap GetImageBitmapFromUrl(string url)
        {
            try
            {
                Bitmap imageBitmap = null;
                using (var webClient = new WebClient())
                {
                    var imageBytes = webClient.DownloadData(url);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }
                return imageBitmap;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Erro GetIMageBitmapFromUrl: " + ex.Message);
                return null;
            }
        }
    }

    public class Source
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
    }

}