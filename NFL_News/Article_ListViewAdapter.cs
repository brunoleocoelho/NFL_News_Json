﻿/***
 * propriedades
 * Count – To tell the control how many rows are in the data.
 * GetView – To return a View for each row, populated with data. This method has a parameter for the ListView to pass in an existing, unused row for re-use.
 * GetItemId – Return a row identifier (typically the row number, although it can be any long value that you like).
 * this[int] indexer – To return the data associated with a particular row number.
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace NFL_News
{
    class Article_ListViewAdapter : BaseAdapter<Article>
    {
        private List<Article> articles;
        private Context context;

        //construtor recebe o contexto, e a lista de itens
        public Article_ListViewAdapter(Context ctxt, List<Article> art)
        {
            this.articles = art;
            this.context = ctxt;
        }
        
        //To return the data associated with a particular row number.
        public override Article this[int position] => articles[position];
        
        //To tell the control how many rows are in the data.
        public override int Count => articles.Count;

        //Return a row identifier (typically the row number, 
        //although it can be any long value that you like)
        public override long GetItemId(int position) => position;

        //To return a View for each row, populated with data. This method has a parameter 
        //for the ListView to pass in an existing, unused row for re-use.
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
            {
                view = LayoutInflater.From(context).Inflate(Resource.Layout.row_Article, null);
            }

            if (articles != null)
            {
                TextView txtTitle = view.FindViewById<TextView>(Resource.Id.txtTitle);
                txtTitle.Text = articles[position].title;

                TextView txtDescription = view.FindViewById<TextView>(Resource.Id.txtDescription);
                txtDescription.Text = articles[position].description.Trim();

                TextView txtDtHr = view.FindViewById<TextView>(Resource.Id.txtDtHr);
                txtDtHr.Text = articles[position].publishedAt.ToLongDateString();

                ImageView imgPicture = view.FindViewById<ImageView>(Resource.Id.imgPicture);
                var imgBitmap = new Article().GetImageBitmapFromUrl(articles[position].urlToImage);
                if (imgBitmap != null)
                    imgPicture.SetImageBitmap(imgBitmap);
                else
                    imgPicture.SetImageResource(Resource.Drawable.nfl_icon_red);
            }
            return view;
        }
    }
}