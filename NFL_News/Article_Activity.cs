﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NFL_News
{
    [Activity(Label = "Article_Activity", Icon = "@drawable/nfl_icon_blue")]
    public class Article_Activity : Activity
    {
        private TextView txtTitle;
        private TextView txtDtHr;
        private TextView txtDescription;
        private ImageView imgPicture;
        private Button btnVerNoSite;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Article);

            //pegando os valores passados da activity anteiror
            var array = Intent.GetStringArrayExtra("artigo");
            var article = new Article
            {
                author = array[0],
                description = array[1],
                publishedAt = DateTime.Parse(array[2]),
                title = array[3],
                url = array[4],
                urlToImage = array[5],
                source = new Source { id="", name=""}
            };

            txtTitle = FindViewById<TextView>(Resource.Id.txtTitle);
            txtTitle.Text = article.title;

            txtDtHr = FindViewById<TextView>(Resource.Id.txtDtHr);
            txtDtHr.Text = article.publishedAt.ToLongDateString();

            txtDescription = FindViewById<TextView>(Resource.Id.txtDescription);
            txtDescription.Text = article.description;

            imgPicture = FindViewById<ImageView>(Resource.Id.imgPicture);
            var imgBitmap = new Article().GetImageBitmapFromUrl(article.urlToImage);
            if (imgBitmap != null)
                imgPicture.SetImageBitmap(imgBitmap);

            btnVerNoSite = FindViewById<Button>(Resource.Id.btnVerNoSIte);
            btnVerNoSite.Click += (sender, e) =>
            {
                //abre no navegador
                Intent httpIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(article.url));
                StartActivity(httpIntent);
            };
        }
    }
}